﻿using Autonomy.Characters.Species;

namespace Autonomy.Characters.Traits
{
	/// <summary>
	/// Base Personality <see cref="Trait"/>, that is restricted to humans only.
	/// </summary>
	public class TraitPersonality : Trait
	{
		public TraitPersonality()
		{
			species_restricted = CharacterSpecies.SpeciesID.Human;
		}
	}
}
