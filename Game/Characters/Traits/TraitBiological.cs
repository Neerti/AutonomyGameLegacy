﻿using Autonomy.Characters.Species;

namespace Autonomy.Characters.Traits
{
	/// <summary>
	/// Base Trait for biological alterations applied to humans.
	/// </summary>
	public class TraitBiological : Trait
	{
		public TraitBiological()
		{
			species_restricted = CharacterSpecies.SpeciesID.Human;
		}
	}
}