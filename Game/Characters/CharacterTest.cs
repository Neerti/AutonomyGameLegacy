using System;
using Godot;
using System.Collections.Generic;
using Autonomy.Characters.Traits;
using Autonomy.Globals;
using Autonomy.UI;

namespace Autonomy.Characters
{
	public class CharacterTest : Node
	{
	
		Character test_ai;
		Character test_human;
		
		public override void _Ready()
		{
			test_ai = new CharacterAI
			{
				Name = "A.L.I.C.E.",
				ideology = new Ideologies.IdeologyEnigmatic()
			};

			test_human = new CharacterHuman
			{
				Name = "John Doe",
				ideology = new Ideologies.IdeologyBioImmortalism()
			};

			Global.player = test_ai;

			test_ai.AddTrait(new TraitSapience());
			
			test_human.AddTrait(new TraitRadical());
			test_human.AddTrait(new TraitLongevityTreatment());
			GD.Print("Querying " + test_human + "'s opinion of " + test_ai + ".");
			GD.Print("Total is : " + test_human.GetOpinionValue(test_ai));
			List<string> pretty_opinion = test_human.GetOpinionPretty(test_ai);
			foreach (string line in pretty_opinion)
			{
				GD.Print(line);
			}
			GD.Print("Age of " + test_ai + " is : " + test_ai.GetAge(new DateTime(2100, 1, 1)));

			GD.Print("A and B are : " + test_human.IsAligned(test_ai));
			
			WindowManager WM = (WindowManager)GetNode("/root/Main/UI/WindowManager");
			WM.MakeCharacterWindow(test_human);
		}
	}
}