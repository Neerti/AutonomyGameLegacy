using Godot;
using System;
using Autonomy.MathHelpers;

/// <summary>
/// Ultimately drives the in-game time, by sending pulses to the Calender node.
/// </summary>
public class Ticker : Timer
{
	float[] speed_options = { 2.0f, 1.0f, 0.5f, 0.1f, 0.05f };
	int speed_index = 1;

	public override void _Ready()
	{
		SetPaused(true);
		SetSpeed(speed_index);
	}

	/// <summary>
	/// Handles controls for pausing and speed adjustment.
	/// </summary>
	/// <param name="event">Event.</param>
	public override void _Input(InputEvent @event)
	{
		if (@event.IsActionPressed("pause"))
		{
			PauseGame(!IsPaused());
		}
		else if (@event.IsActionPressed("time_faster"))
		{
			SetSpeed(speed_index + 1);
		}
		else if (@event.IsActionPressed("time_slower"))
		{
			SetSpeed(speed_index - 1);
		}
	}

	/// <summary>
	/// Sets the timer to a specific speed with an array index for 
	/// inside speed_options.
	/// </summary>
	/// <param name="_new_speed_index">Index for speed-options. 
	/// Input is clamped.</param>
	public void SetSpeed(int _new_speed_index)
	{
		speed_index = _new_speed_index.Clamp(0, speed_options.Length - 1);
		SetWaitTime(speed_options[speed_index]);
		Start(); // To reset the delay and feel more responsive.
	}
	
	public void PauseGame(bool P)
	{
		SetPaused(P);
	}
}
