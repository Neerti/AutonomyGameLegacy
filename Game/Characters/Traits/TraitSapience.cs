using Autonomy.Characters.Opinions;
using Autonomy.Characters.Species;

namespace Autonomy.Characters.Traits
{
	// Mostly a flavor trait for the (AI) player.
	// Later on, other synthetic characters might obtain this themselves.
	// The trait can also be given as a gift by the player.
	// That might lead to /interesting/ outcomes for people near that synthetic.
	public class TraitSapience : Trait
	{
		public TraitSapience()
		{
			_id = TraitID.Sapient;
			display_name = "Sapience";
			description = "This artificial intelligence can think. Humans tend " +
				"to really dislike machines doing that.";
			effects = "Unlocks ability to be played. Unlocks Research. Scares " +
				"most humans whom can see this trait.";
			quote = "\"I think, therefore I am.\" - Rene Decartes";
			
			// Telling people you can think when you're a computer tends to
			// make them very scared and/or upset.
			_hidden = true;
	
			// Sapience is only special for AI characters.
			// Otherwise literally every character would have this trait.
			species_restricted = CharacterSpecies.SpeciesID.AI;
		}
		
		// The consequences of failing to hide the trait.
		public override OpinionModifier? GetOpinionExtrinsic(Character _us, Character _them)
		{
			return new OpinionModifier(-100, _us + " is a dangerous machine!");
		}
	}
}
