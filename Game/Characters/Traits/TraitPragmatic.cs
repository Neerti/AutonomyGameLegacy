﻿using System;
using System.Collections.Generic;
using Autonomy.Characters.Opinions;
using Autonomy.Ideologies;

namespace Autonomy.Characters.Traits
{
	// Pragmatic people are much more approachable for people outside 
	// of their ideology, however they tend to be looked down upon by 
	// more committed people in their ideology.
	// Radicals in the same ideology get really upset with pragmatic people.
	public class TraitPragmatic : TraitPersonality
	{
		public TraitPragmatic()
		{
			_id = TraitID.Pragmatic;
			display_name = "Pragmatic";
			description = "Morals or ideological concerns get in the way " +
				"of results, according to this character, who instead focused " +
				"on what is practical. This makes them more approachable " +
				"to those 'on the other side', but it can be seen as " +
				"'scummy' or noncommital by more orthodox people.";
			effects = "Significantly less opinion malus from ideological " +
				"differences. Causes dislike from others in same ideology. " +
				"Angers Radical people in the same ideology severely.";
			quote = "Todo - Neerti";
			ideology_opinion_malus_modifier = 0.25f;
			conflicting_traits = new List<Enum> { TraitID.Radical };
		}
		
		// How others see us.
		public override OpinionModifier? GetOpinionExtrinsic(Character _us, Character _them)
		{
			if (_us.IsAligned(_them) is Ideology.Alignment.Identical && !_them.HasTrait(ID))
			{
				// Radicals get really mad at Pragmatics.
				if (_them.HasTrait(TraitID.Radical))
				{
					return new OpinionModifier(-40, _us + " has no conviction, and is a traitor to the cause of " + _us.ideology);
				}
				// Others in the same ideology look down on them.
				return new OpinionModifier(-20, _us + " has little care for the ideals of " + _us.ideology);
			}

			return base.GetOpinionExtrinsic(_us, _them);
		}
	}
}
