﻿using System;

namespace Autonomy.Ideologies
{
	/// <summary>
	/// An <see cref="IdeologyGroup"/> categorizes and groups similar types of 
	/// <see cref="Ideology"/> types. <see cref="Characters.Character"/>s that are not in 
	/// the same ideology but are in the same group will dislike each other 
	/// less than they would normally have.
	/// </summary>
	public abstract class IdeologyGroup
	{
		// UI/Bookkeeping things.
		public enum IdeologyGroupID
		{
			Base,
			Unaligned,
			Transhumanism,
			Bioconservatism,
			Assimilated
		}
		
		/// <summary>
		/// Unique enum ID for bookkeeping.
		/// </summary>
		protected Enum id = IdeologyGroupID.Base;
	
		public Enum ID => id;
	
		// Todo: Interface for this & traits?
		/// <summary>
		/// The name of the Ideology that is shown to the player.
		/// </summary>
		protected string display_name;
	
		public string Display_name => display_name;
		
		public override string ToString()
		{
			return display_name;
		}
	
		/// <summary>
		/// A description about the ideology group associated with this ideology.
		/// </summary>
		protected string description;
	
		public string Description => description;
	}
}
