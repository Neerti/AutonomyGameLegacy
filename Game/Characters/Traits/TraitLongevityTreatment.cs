﻿using System;
using Autonomy.Characters.Opinions;

namespace Autonomy.Characters.Traits
{
	// Makes the character have a longer life than normal.
	// Those without the same trait will feel some resentment against them.
	// That isn't much of an issue if the society the character lives in gives 
	// everyone the treatment, but if they don't...
	public class TraitLongevityTreatment : TraitBiological
	{
		public TraitLongevityTreatment()
		{
			_id = TraitID.Longevity;
			display_name = "Longevity Treatment";
			description = "This person has received a specialized genetic " +
				"treatment that will reduce the effects of aging, allowing " +
				"them to potentially live a longer life.";
			effects = "Increases lifespan by 30 years on average. Become " +
				"disliked by others without the same trait.";
			lifespan_modifier = 30;
		}
		
		// Receiving the treatment can make others get jealous of your longer life.
		public override OpinionModifier? GetOpinionExtrinsic(Character _us, Character _them)
		{
			if (!_them.HasTrait(_id))
			{
				return new OpinionModifier(-20, _us + " will live longer than me.");
			}
			return base.GetOpinionExtrinsic(_us, _them);
		}
	}
}
