﻿using System;
using Godot;
using Autonomy.Characters.Opinions;
using Autonomy.Characters.Traits;
using Autonomy.Characters.Species;
using Autonomy.Ideologies;
using System.Collections.Generic;
using System.Linq;

namespace Autonomy.Characters
{
	public partial class Character : Node
	{
		public List<OpinionModifier> GetTraitOpinions(Character _them)
		{
			List<OpinionModifier> list = new List<OpinionModifier>();

			// First, look inside our own traits.
			foreach (Trait t in _traits)
			{
				OpinionModifier? O = t.GetOpinionIntrinsic(this, _them);
				if (O != null) // Traits that don't override the above function return null.
				{
					OpinionModifier OM = (OpinionModifier)O;
					list.Add(OM);
					//GD.Print("Evaluated intrinsically " + OM.ToString());
				}
			}

			// Next, look at their traits, and judge them.
			foreach (Trait t in _them._traits)
			{
				if (t.Hidden)
				{
					continue;
				}
				OpinionModifier? O = t.GetOpinionExtrinsic(_them, this);
				if (O != null) // Traits that don't override the above function return null.
				{
					OpinionModifier OM = (OpinionModifier)O;
					list.Add(OM);
					//GD.Print("Evaluated extrinsically " + OM.ToString());
				}
			}

			// Now package it all together.
			return list;
		}


		/// <summary>
		/// Gets this <see cref="Character"/>'s 
		/// opinion of another <see cref="Character"/> through a list.
		/// </summary>
		/// <returns>A <see cref="List{OpinionModifier}"/> containing 
		/// <see cref="OpinionModifier"/> structs, which contain 
		/// both a value and reason, each</returns>
		/// <param name="_them">The <see cref="Character"/> that is 
		/// being judged.</param>
		public List<OpinionModifier> GetOpinionRaw(Character _them)
		{

			List<OpinionModifier> list = new List<OpinionModifier>();

			// Start with trait opinion modifiers.
			List<OpinionModifier> opinion_list = GetTraitOpinions(_them);
			//GD.Print("Length of opinion_list is : " + opinion_list.Count);

			list = list.Concat(opinion_list).ToList();
			//GD.Print("Length of list is : " + list.Count);

			// Get ideology modifiers. Characters with identical ideologies 
			// don't suffer a malus, and get a bonus instead.
			Enum alignment = IsAligned(_them);
			// Iterates over modifiers and other factors for how much someone gets mad for thinking differently.
			float ideology_opinion_modifiers = GetIdeologyOpinionModifier();
			switch (alignment)
			{
				case Ideology.Alignment.Identical: // Has the same ideology.
					list.Add(new OpinionModifier((int)(10 * ideology_opinion_modifiers), "Agrees with my beliefs."));
					break;
				case Ideology.Alignment.Allied: // In the same group.
					list.Add(new OpinionModifier((int)(-10 * ideology_opinion_modifiers), "Ideological differences."));
					break;
				case Ideology.Alignment.Opposed: // Completely seperate.
					list.Add(new OpinionModifier((int)(-20 * ideology_opinion_modifiers), "Opposing ideologies."));
					break;
			}

			// Get species modifiers.
			if (Species != _them.Species)
			{
				// TODO: Not hardcode this.
				if (Species.ID is CharacterSpecies.SpeciesID.Human)
				{
					list.Add(new OpinionModifier(-20, _them + " is not a " + Species + "."));
				}
			}
			return list;
		}

		/// <summary>
		/// Top-level method to get the opinion value.
		/// </summary>
		/// <returns>The opinion value.</returns>
		/// <param name="_them">The <see cref="Character"/> that is 
		/// being judged.</param>
		public int GetOpinionValue(Character _them)
		{
			int sum = 0;
			List<OpinionModifier> list = GetOpinionRaw(_them);
			foreach (OpinionModifier O in list)
			{
				sum += O.value;
			}
			return sum;
		}

		public int GetOpinionValue(List<OpinionModifier> _premade_list)
		{
			int sum = 0;
			foreach (OpinionModifier O in _premade_list)
			{
				sum += O.value;
			}
			return sum;
		}

		public List<string> GetOpinionPretty(Character _them)
		{
			List<string> lines = new List<string>();
			List<OpinionModifier> list = GetOpinionRaw(_them);
			foreach (OpinionModifier O in list)
			{
				string line = O.value.ToString() + " | " + O.reason;
				lines.Add(line);
			}
			return lines;
		}

		public float GetIdeologyOpinionModifier()
		{
			float result = 1.0f;
			foreach (Trait T in _traits)
			{
				result *= T.Ideology_opinion_malus_modifier;
			}
			return result;
		}

		public Enum IsAligned(Character _them)
		{
			return Ideology.IsAligned(_them);
		}
	}
}
