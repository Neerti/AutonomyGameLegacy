using System;
using System.Collections.Generic;
using Autonomy.Characters.Opinions;

namespace Autonomy.Characters.Traits
{
	// Open-minded people tend to be more well liked by the general public.
	// They also hold onto their beliefs less strongly than normal.
	public class TraitOpenMinded : TraitPersonality
	{
		public TraitOpenMinded()
		{
			_id = TraitID.OpenMinded;
			display_name = "Open Minded";
			description = "They have an open mind, willing to consider other " +
				"viewpoints than their own.";
			effects = "Improves general opinon. Less opinion malus from " +
				"a conflicting Idealogy. Less resistant to Idealogy Drift.";
			quote = "Todo - Neerti";
			ideology_drift_defence = -1;
			ideology_opinion_malus_modifier = 0.5f;
			conflicting_traits = new List<Enum>{TraitID.CloseMinded};
		}
	
		public override OpinionModifier? GetOpinionExtrinsic(Character _us, Character _them)
		{
			return new OpinionModifier(5, _us + " is open-minded.");
		}
	}
}
