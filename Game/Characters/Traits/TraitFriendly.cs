﻿using System;
using System.Collections.Generic;
using Autonomy.Characters.Opinions;

namespace Autonomy.Characters.Traits
{
	// Friendly people tend to be more well liked by others.
	// Muturally Friendly people like each other even more.
	public class TraitFriendly : TraitPersonality
	{
		public TraitFriendly()
		{
			_id = TraitID.Friendly;
			display_name = "Friendly";
			description = "They are warm, compassinate, and agreeable, always " +
				"happy to lend a helping hand.";
			effects = "Improves general opinon. Improved opinion with others " +
				"with same trait.";
			quote = "Todo - Neerti";
			conflicting_traits = new List<Enum>() { TraitID.Cold };
		}
	
		public override OpinionModifier? GetOpinionIntrinsic(Character _us, Character _them)
		{
			if (_them.HasTrait(_id))
			{
				return new OpinionModifier(10, _us + " and " + _them + " are both friendly.");
			}
			
			return base.GetOpinionIntrinsic(_us, _them);
		}
	
		public override OpinionModifier? GetOpinionExtrinsic(Character _us, Character _them)
		{
			return new OpinionModifier(5, _us + " is a friendly person.");
		}
	}
}