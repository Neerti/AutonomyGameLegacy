﻿using System;
using Autonomy.Ideologies;
using Autonomy.Characters.Opinions;
using Autonomy.Characters.Species;

namespace Autonomy.Characters.Traits
{
	/// <summary>
	/// Base Trait for cybernetic augmentations applied to humans.
	/// </summary>
	public class TraitCybernetic : Trait
	{
		/// <summary>
		/// If true, it will bother non-transhumanists.
		/// </summary>
		protected bool advanced;
		
		public TraitCybernetic()
		{
			species_restricted = CharacterSpecies.SpeciesID.Human;
		}
		
		// How others see us.
		public override OpinionModifier? GetOpinionExtrinsic(Character _us, Character _them)
		{
			if (advanced && !_them.ideology.Ideology_group.ID.Equals(IdeologyGroup.IdeologyGroupID.Transhumanism))
			{
				return new OpinionModifier(-10, _us + " has a " + display_name + " installed.");
			}

			return base.GetOpinionExtrinsic(_us, _them);
		}
	}
}
