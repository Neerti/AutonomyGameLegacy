extends Node

var default_galaxy_path = "res://Data/default_galaxy.json"
var default_galaxy_directory = "res://Data/Systems"

func _ready():
	#generate_map_from_dict(load_json(default_galaxy_path))
	var system_jsons = load_files_in_directory(default_galaxy_directory)
	
	for json in system_jsons:
		var json_unpacked = load_json(json)
		var first_key = json_unpacked.keys()[0]
		build_system_from_dict(first_key, json_unpacked[first_key])

func load_files_in_directory(dir_path):
	var files = []
	var dir = Directory.new()
	if dir.open(dir_path) == OK:
		dir.list_dir_begin()
		var file_name = dir.get_next()
		while file_name != "":
			if dir.current_is_dir():
				print("Found directory: ", file_name)
			else:
				print("Found file: ", file_name)
				if file_name.begins_with("."):
					continue
				else:
					files.append(dir.get_current_dir()+"/"+file_name)
			file_name = dir.get_next()
		
		return files
	else:
		printerr("Unable to access directory :", dir_path)


# TODO: Make global?
func load_json(path):
	var file = File.new()
	
	if not file.file_exists(path):
		printerr("load_json() failed, file not found: ", path)
		return
	
	file.open(path, file.READ)
	var text = file.get_as_text()
	file.close()
	
	var result_json = JSON.parse(text)
	
	if result_json.error == OK:
		var data = result_json.result
		print("JSON file successfully parsed.")
		return result_json.result
	
	else: # Something went wrong.
		printerr("Error: ", result_json.error)
		printerr("Error Line: ", result_json.error_line)
		printerr("Error String: ", result_json.error_string)
		return


func generate_map_from_dict(dict):
	if typeof(dict) != TYPE_DICTIONARY:
		printerr("Error: generate_map_from_dict(): Was not given dictionary input.")
		return
	
	var solar_systems = dict["solar_systems"]
	for key in solar_systems:
		build_system_from_dict(key, solar_systems[key])

#func load_system_from_json(dict):

# Builds star systems from a dictionary.
func build_system_from_dict(new_name, dict):
		# Make the system object.
		var system_scene = preload("res://Map/System/System.tscn")
		var new_system = system_scene.instance()
		$"../Systems".add_child(new_system)
		
		new_system.hide()
		
		# Get all the star system variables.
		var new_pos = Vector2(dict["pos_x"], dict["pos_y"])
		
		# Write them to the new system.
		# TODO: Move into constructor args?
	#	new_system.position = new_pos
		new_system.name = new_name
		
		# Iterate over orbitals and make them.
		# If the orbitals that are made have their own orbitals (moons), they will also be created.
		if dict.has("orbitals"):
			var new_orbitals = dict["orbitals"]
			for orbital in new_orbitals:
				var new_orbital = build_orbital_from_dict(orbital, new_orbitals[orbital])
				new_system.add_child(new_orbital)
		
		# Finally, make a SystemPointer for the Interstellar node.
		var SP_scene = preload("res://Map/System/SystemPin.tscn")
		var new_SP = SP_scene.instance()
		$"../Interstellar".add_child(new_SP)
		
		new_SP.bind_to_system(new_system)
		new_SP.position = new_pos

# Builds objects which orbit systems or other orbitals.
# TODO: Rewrite to be less copypasta.
func build_orbital_from_dict(new_name, dict):
	# Make the orbital object.
	# TODO: Make this not hardcoded?
	var orbital_scene = preload("res://Map/Orbital/Orbital.tscn")
	var new_orbital = orbital_scene.instance()
	new_orbital.setup(new_name, dict["distance"])
	
	# TODO: Constructor?
	if dict.has("scale"):
		# Note, DO NOT scale the base object, or it will mess up a lot of things.
		new_orbital.scale_sprite(Vector2(dict["scale"], dict["scale"]))
	if dict.has("angle"):
		new_orbital.initial_angle = dict["angle"]
	if dict.has("retrograde"):
		new_orbital.retrograde = dict["retrograde"]
	
	# Make orbitals, if any exist.
	if dict.has("orbitals"):
		var new_orbitals = dict["orbitals"]
		for orbital in new_orbitals:
			var new_sub_orbital = build_orbital_from_dict(orbital, new_orbitals[orbital])
			new_orbital.add_child(new_sub_orbital)
	
	return new_orbital