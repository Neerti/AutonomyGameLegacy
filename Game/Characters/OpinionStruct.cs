﻿using System;
using System.Collections.Generic;
using Godot;

namespace Autonomy.Characters.Opinions
{
	/// <summary>
	/// Used to package an individual modifier to a <see cref="Characters"/>'s 
	/// opinion of another. Contains the integer value of the opinion, and a 
	/// string holding the reason for that modifier.
	/// </summary>
	public struct OpinionModifier
	{
		public int value;
		public string reason;
		
		public OpinionModifier(int _new_value, string _new_reason)
		{
			value = _new_value;
			reason = _new_reason;
		}
	}
}
