extends Label

# class member variables go here, for example:
# var a = 2
# var b = "textvar"

func _ready():
#	$"../Ticker".connect("timeout", self, "tick")
	$"/root/Main/Time/Calender".connect("DateChanged", self, "update_date_text")
	pass

func update_date_text(date_string):
	text = date_string
