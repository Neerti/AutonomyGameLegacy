using Godot;
using System;
using Autonomy.Characters;
using Autonomy.UI.Windows;

namespace Autonomy.UI
{
	public class WindowManager : CanvasLayer
	{
		public override void _Ready()
		{
			GD.Print("WindowManager is _Ready()!");
		}

		public void MakeCharacterWindow(Character _new_subject)
		{
			PackedScene character_window_scene = (PackedScene)ResourceLoader.Load("res://UI/Windows/CharacterWindow.tscn");
			Window window = (Window)character_window_scene.Instance();
			
			CharacterWindowContainer CWC = (CharacterWindowContainer)window.GetNode("Content/Body");
			CWC.SetSubject(_new_subject);
			AddChild(window);
			
		}
	}
}