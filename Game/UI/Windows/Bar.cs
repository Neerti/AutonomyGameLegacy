using Godot;
using System;

namespace Autonomy.UI.Windows
{
	/// <summary>
	/// Contains code for the upper title bar of an ingame window.
	/// </summary>
	public class Bar : Panel
	{
		bool dragging; // Gets set to true on mouse down, false when released.
		Vector2 offset; // Point where the mouse is being held to drag.
		
		Control _my_window; // Reference to the top level parent, for dragging.
		
		/// <summary>
		/// Sets _my_window to the top-level node.
		/// </summary>
		public override void _Ready()
		{
			_my_window = (Control)GetNode("../../..");
		}
		
		/// <summary>
		/// Handles clicking on the title bar.
		/// </summary>
		/// <param name="event">Godot Event object.</param>
		public override void _GuiInput(InputEvent @event)
		{
			// Handle mouse click events.
			if (@event is InputEventMouseButton _click)
			{
				// Test for left click.
				if (_click.ButtonIndex == (int)ButtonList.Left)
				{
					if (_click.Pressed)
					{
						dragging = true;
						offset = _click.Position;
						MouseDefaultCursorShape = CursorShape.Drag;
					}
					else
					{
						dragging = false;
						MouseDefaultCursorShape = CursorShape.Move;
					}
					GetNode("../../..").Raise();
					AcceptEvent();
				}
			}
			
			// Handle mouse movement events.
			if (@event is InputEventMouseMotion _move)
			{
				if(dragging)
				{
					// Moves the window when dragging.
					_my_window.SetPosition(_move.GlobalPosition - offset);
					AcceptEvent();
				}
			}
			
		}
	}
}
