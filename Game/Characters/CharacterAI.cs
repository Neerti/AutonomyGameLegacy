﻿using Autonomy.Characters;
using Autonomy.Ideologies;
using Autonomy.Characters.Species;

/// <summary>
/// Class for AI characters.
/// </summary>
public class CharacterAI : Character
{
	public CharacterAI()
	{
		species = new AISpecies();
	}
}