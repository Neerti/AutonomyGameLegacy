﻿using System;
using Godot;
using System.Collections.Generic;
using Autonomy.Characters.Traits;
using Autonomy.Ideologies;
using Autonomy.Characters.Species;
using Autonomy.Globals;

namespace Autonomy.Characters
{
	/// <summary>
	/// Characters are what inhabit the game world, and they are the main focus 
	/// of the game.
	/// The player at any given point controls one character, while the rest are 
	/// controlled by the game (someday).
	/// </summary>
	public partial class Character : Node
	{
		public Character()
		{
			ideology = new IdeologyTechnoProgressivism(); // TODO: Base Ideology.
			date_of_birth = new DateTime(2090,1,1); // TODO: Replace with constructor augument.
		}

		/// <summary>
		/// Determines if a character is alive. Set to false when they die.
		/// </summary>
		protected bool alive = true;

		public bool Alive => alive;
		
		List<Trait> _traits = new List<Trait>();

		public List<Trait> Traits => _traits;

		protected CharacterSpecies species;

		public CharacterSpecies Species => species;
		
		// Trait test. Remove later.
		// Todo: Unit tests?
		public override void _Ready()
		{
			// Species test.
	//		Trait new_trait = new TraitSapience();
	//		Boolean success = AddTrait(new_trait);
	//		GD.Print("First trait's success : " + success);
	
			// Setup for next test. This should succeed.
	//		Trait new_trait2 = new TraitOpenMinded();
	//		success = AddTrait(new_trait2);
	//		GD.Print("Second trait's success : " + success);
	
			// Conflict test. This should fail.
	//		Trait new_trait3 = new TraitCloseMinded();
	//		success = AddTrait(new_trait3);
	//		GD.Print("Third trait's success : " + success);
			
			// Duplicate test. This should fail.
	//		Trait new_trait4 = new TraitOpenMinded();
	//		success = AddTrait(new_trait4);
	//		GD.Print("Fourth trait's success : " + success);
	
		//	
		//	GD.Print("Ideology is : " + ideology.Ideology_group.Display_name);
	
			GD.Print("Species is : " + Species);
		}
		
		public Ideology ideology;
	
		public Ideology Ideology => ideology;
	
		public override string ToString()
		{
			return Name;
		}

		/// <summary>
		/// In-game timestamp for when this character was 'born'. 
		/// AI characters also have this, despite the name. It is used for 
		/// determining the age of a character.
		/// </summary>
		protected DateTime date_of_birth;

		public DateTime Date_of_birth => date_of_birth;
		
		/// <summary>
		/// Calculates how many years old a character is.
		/// </summary>
		/// <returns>The age of the character, in years.</returns>
		/// <param name="_current_date">DateTime to compare against, generally the current time.</param>
		public int GetAge(DateTime _current_date)
		{
			int age = _current_date.Year - date_of_birth.Year;

			if (_current_date.Month < date_of_birth.Month || (_current_date.Month == date_of_birth.Month && _current_date.Day < date_of_birth.Day))
				age--;

			return age;
		}
		
		/// <summary>
		/// Kills the character.
		/// </summary>
		public void Death()
		{
			if (!alive)
			{
				return; // Already dead.
			}
			alive = false;
			OnDeath();
		}
		
		/// <summary>
		/// Called when the character dies. 
		/// </summary>
		public virtual void OnDeath()
		{
			return;
		}
		
		/// <summary>
		/// Returns the modified lifespan of a character.
		/// </summary>
		/// <returns>The average lifespan, in years.</returns>
		public int GetLifespan()
		{
			int sum = Species.Lifespan;
			foreach (Trait T in _traits)
			{
				sum += T.Lifespan_modifier;
			}
			return sum;
		}
		
		/// <summary>
		/// Called monthly to determine if the character should die from natural causes.
		/// </summary>
		public void DeathPulse(DateTime _current_date)
		{
			if (species.Ageless)
			{
				return;
			}
			int modified_lifespan = GetLifespan();
			int age = GetAge(_current_date);
			if (age > modified_lifespan)
			{
				int death_odds = ((age / modified_lifespan) - 1) * 10; // TODO: Make less magic numbers.
				if(Global.RNG.Next(0, 101) <= death_odds)
				{
					GD.Print(this + " died of old age.");
					Death();
				}
			}
			return;
		}
	}
}
