extends Label

# class member variables go here, for example:
# var a = 2
# var b = "textvar"
var map_object = null
var camera_object = null

func _ready():
	map_object = $"/root/Main/Map"
	camera_object = $"/root/Main/Camera"

func _process(delta):
	if map_object.inside_system_map:
		text = "System View (" + map_object.current_system.name + " )"
		text += "\n"
		text += str(camera_object.pixels_per_AU) + " pixels/AU"
	else:
		text = "Interstellar View"
	

