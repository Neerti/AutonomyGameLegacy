﻿namespace Autonomy.Ideologies
{
	public class IdeologyTranshumanism : Ideology
	{
		public IdeologyTranshumanism()
		{
			_ideology_group = new IdeologyGroupTranshumanism();
		}
	}
	
	public class IdeologyGroupTranshumanism : IdeologyGroup
	{
		public IdeologyGroupTranshumanism()
		{
			id = IdeologyGroupID.Transhumanism;
			display_name = "Transhumanism";
			description = "Transhumanism is a movement that aims to transform, " +
				"and in some cases, redefine the human condition, through the " +
				"use of science and advanced technology.";
		}
	}
	
	public class IdeologyTechnoProgressivism : IdeologyTranshumanism
	{
		public IdeologyTechnoProgressivism()
		{
			id = IdeologyID.TechnoProgressivism;
			display_name = "Techno-Progressivism";
			description = "Techno-Progressivism supports the use and development " +
				"of science and technology in order to advance humanity both " +
				"technologically, and socially. It argues that the consequences " +
				"of new technologies are often simply new, smaller obstacles " +
				"that can be overcome through further advancement. " +
				"Techno-Progressivists believe that scientific and technical " +
				"advancment can lead to progress for humanity as a whole, " +
				"allowing for longer lives, less disease, a greater quality " +
				"of life, and the end of suffering. As such, it is the most " +
				"popular of the transhumanism movements.";
			effects = "Gives a minor boost to all research speed.";
			quote = "\"The future cannot be predicted, but futures " +
				"can be invented.\" - Dennis Gabor, Inventing the Future (1963)";
		}
	}
	
	public class IdeologyBioImmortalism : IdeologyTranshumanism
	{
		public IdeologyBioImmortalism()
		{
			id = IdeologyID.BioImmortalism;
			display_name = "Bio-Immortalism";
			description = "For as long as humans have lived, they have tried to " +
				"escape their mortality. While they have never been successful, " +
				"people subscribing to Bio-Immortalism believe that they are " +
				"within reach of conquering their mortality, or at the very " +
				"least, prolonging their life, through the radical application " +
				"of genetic and biological technology.";
			effects = "Gives a moderate boost to biological-related " +
				"research speed.";
			quote = "\"Death must be an evil — and the gods agree; " +
				"for why else would they live for ever?\" - Sappho (600 B.C.)";
	
		}
	}
	
	public class IdeologyCyberneticism : IdeologyTranshumanism
	{
		public IdeologyCyberneticism()
		{
			id = IdeologyID.Cyberneticism;
			display_name = "Cyberneticism";
			description = "Cyberneticism is the belief that an organic form is " +
				"inherently inferior to a cybernetic replacement. " +
				"Cyberneticists seek to design, develop, install, and maintain " +
				"cybernetic augmentations and replacements for their body. " +
				"Some hope to be able to replace their entire body " +
				"(sans the brain) and become almost perfect machines, " +
				"being able to enjoy a very long life and becoming greater " +
				"than any unaugmented human ever could.";
			effects = "Gives a moderate boost to cybernetic-related " +
				"research speed.";
			quote = "Todo";
		}
	}
	
	public class IdeologySingulatarianism : IdeologyTranshumanism
	{
		public IdeologySingulatarianism()
		{
			id = IdeologyID.Singulatarianism;
			display_name = "Singulatarianism";
			description = "Singularitarianism is an offshoot of Transhumanism, " +
				"which believes that the Singularity, a point in time where the " +
				"rate of technological growth reaches unimaginable levels, " +
				"driven by an artificial superintelligence, will occur in the " +
				"future. Singularitarians wish to both ensure that the " +
				"Singularity is allowed to occur, as well as ensuring that " +
				"the Singularity benefits humanity.";
			effects = "No hostility to sapient AIs.";
			quote = "\"It may be that our role on this planet is not to worship " +
				"God - but to create him.\" - Arthur C. Clarke";
		}
	}
}