extends Label

# TODO: Use signals instead for performance/less coupling.
func _ready():
	update_text()

func _process():
	update_text()

func update_text():
	var my_character = Globals.player_character
	if my_character:
		text = my_character.name
	else:
		text = "(Observer)"