extends Timer

# Ultimately drives the in-game time, by sending pulses to the Calender node.

var speed_index = 1
var possible_speeds = [2, 1, 0.5, 0.1, 0.05]

func _ready():
	pause_game(true)
	set_speed(speed_index)

# Handle controls for pausing and speed adjustment.
func _input(event):
	if event.is_action_pressed("pause"):
		pause_game(!is_paused())
	
	elif event.is_action_pressed("time_faster"):
		set_speed(speed_index+1)
	
	elif event.is_action_pressed("time_slower"):
		set_speed(speed_index-1)

# Sets the timer to one of possible_speeds by index.
func set_speed(new_speed_index):
	speed_index = clamp(new_speed_index, 0, possible_speeds.size()-1)
	set_wait_time(possible_speeds[float(speed_index)])
	start() # To reset the delay and feel more responsive.
	print("Speed index is :", speed_index)
	print("Wait time is :", wait_time)


func pause_game(P):
	set_paused(P) # Pauses the timer.
#	get_tree().paused = P # Pauses most other things.
