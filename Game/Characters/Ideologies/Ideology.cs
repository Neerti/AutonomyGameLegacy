﻿using System;
using Autonomy.Characters;
using Godot;

namespace Autonomy.Ideologies
{
	/// <summary>
	/// An Ideology is an abstraction for the beliefs and values a 
	/// <see cref="Characters.Character"/> holds at any given time. Interactions between 
	/// characters can be radically altered with ideologies, in a similar way 
	/// as traits. Unlike traits, a character can only hold one 
	/// ideology at a time. 
	/// </summary>
	public abstract class Ideology
	{
		// UI/Bookkeeping things.
		public enum IdeologyID
		{
			Base,
			// Transhumanism
			TechnoProgressivism,
			BioImmortalism,
			Cyberneticism,
			Singulatarianism,
			// Unaligned
			Enigmatic,
			Centrism,
			Apathetic,
			Nonsapient,
		}
		
		/// <summary>
		/// Unique enum ID for bookkeeping.
		/// </summary>
		protected Enum id = IdeologyID.Base;
	
		public Enum ID => id;
	
		protected IdeologyGroup _ideology_group;
	
		public IdeologyGroup Ideology_group => _ideology_group;
		
		public enum Alignment
		{
			Identical,
			Allied,
			Opposed
		}

		/// <summary>
		/// Used to check if two <see cref="Character"/>s follow the same 
		/// ideology, if they're in the same ideology group, or if they're 
		/// completely outside each others' ideologies.
		/// </summary>
		/// <returns>Alignment Enum</returns>
		/// <param name="_them">The <see cref="Character"/> to compare to.</param>
		public Enum IsAligned(Character _them)
		{
			// Test if they have the same ideology.
			if (ID.Equals(_them.Ideology.ID))
			{
				return Alignment.Identical;
			}
			if (Ideology_group.ID.Equals(_them.Ideology.Ideology_group.ID))
			{
				return Alignment.Allied;
			}
			return Alignment.Opposed;
		}
		
		// Todo: Interface for this & traits?
		/// <summary>
		/// The name of the Ideology that is shown to the player.
		/// </summary>
		protected string display_name;
	
		public string Display_name => display_name;
	
		public override string ToString()
		{
			return display_name;
		}
	
		/// <summary>
		/// A description about the Ideology.
		/// </summary>
		protected string description;
	
		public string Description => description;
	
		/// <summary>
		/// Description of mechanical effects the Ideology actually has.
		/// </summary>
		protected string effects;
	
		public string Effects => effects;
	
		/// <summary>
		/// A short quote about the Ideology, for flavor.
		/// </summary>
		protected string quote;
	
		public string Quote => quote;
		
	}
}