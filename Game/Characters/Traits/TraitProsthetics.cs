﻿using System;
using Autonomy.Characters.Species;

namespace Autonomy.Characters.Traits
{
	// These traits are one of a few ways to remove the 'Lost Arm/Leg/etc' traits.
	// Even non-transhumanists don't mind these traits.
	public class TraitProstheticArm : TraitCybernetic
	{
		public TraitProstheticArm()
		{
			_id = TraitID.ProstheticArm;
			display_name = "Prosthetic Arm";
			description = "This character has a prosthetic arm, allowing them " +
				"to regain the functionality they had before the loss of their arm.";
			species_restricted = CharacterSpecies.SpeciesID.Human;
		}
	}
	
	public class TraitProstheticLeg : TraitCybernetic
	{
		public TraitProstheticLeg()
		{
			_id = TraitID.ProstheticLeg;
			display_name = "Prosthetic Leg";
			description = "This character has a prosthetic leg, allowing them " +
				"to regain the functionality they had before the loss of their leg.";
			species_restricted = CharacterSpecies.SpeciesID.Human;
		}
	}
	
	
}