# Holds the date, and sends out appropiate pulses to nodes listening to it.
extends Node

# Signals that occur every so often.
signal daily_pulse
signal monthly_pulse
signal annual_pulse

# Signals that give information in addition to the above.
signal date_changed

var day = 1
var month = OS.MONTH_JANUARY
var year = 2100

var month_names = {
	OS.MONTH_JANUARY : "January",
	OS.MONTH_FEBRUARY : "February",
	OS.MONTH_MARCH : "March",
	OS.MONTH_APRIL : "April",
	OS.MONTH_MAY : "May",
	OS.MONTH_JUNE : "June",
	OS.MONTH_JULY : "July",
	OS.MONTH_AUGUST : "August",
	OS.MONTH_SEPTEMBER : "September",
	OS.MONTH_OCTOBER : "October",
	OS.MONTH_NOVEMBER : "November",
	OS.MONTH_DECEMBER : "December"
	}

var month_shorthand = {
	OS.MONTH_JANUARY : "Jan",
	OS.MONTH_FEBRUARY : "Feb",
	OS.MONTH_MARCH : "Mar",
	OS.MONTH_APRIL : "Apr",
	OS.MONTH_MAY : "May",
	OS.MONTH_JUNE : "Jun",
	OS.MONTH_JULY : "Jul",
	OS.MONTH_AUGUST : "Aug",
	OS.MONTH_SEPTEMBER : "Sep",
	OS.MONTH_OCTOBER : "Oct",
	OS.MONTH_NOVEMBER : "Nov",
	OS.MONTH_DECEMBER : "Dec"
	}

# TODO: Implement user config and read that.
var days_first = false

func _ready():
	$"../Ticker".connect("timeout", self, "tick")

func tick():
	add_day()
	emit_signal("date_changed", get_date_short()) # TODO: Allow player to change date format with config.
#	print(get_date_short_numbers())


func add_day():
	day += 1
	emit_signal("daily_pulse")
	if(day > get_month_length(month)):
		day = 1
		add_month()


func add_month():
	month += 1
	emit_signal("monthly_pulse")
	if(month > OS.MONTH_DECEMBER):
		month = OS.MONTH_JANUARY
		add_year()


func add_year():
	year += 1
	emit_signal("annual_pulse")


func get_month_length(month_to_check):
	# Leap years unfortunately require a lot of extra checks.
	if(month_to_check == OS.MONTH_FEBRUARY):
		var is_leap_year = false
		if(year % 4 == 0): # Every four years is a leap year.
			is_leap_year = true
			if(year % 100 == 0 and not year % 400 == 0): # Except once every 100 years, and it's not once every 400 years.
				is_leap_year = false
		
		if(is_leap_year):
			return 29
		else:
			return 28
	# Now for the normal months.  April, June, September, and November.
	elif(month_to_check == OS.MONTH_APRIL or month_to_check == OS.MONTH_JUNE or month_to_check == OS.MONTH_SEPTEMBER or month_to_check == OS.MONTH_NOVEMBER):
		return 30
	else: #Everything else has 31.
		return 31


func get_ordinal_suffix(number):
	if(number == 0):
		return str(number)+"th" # 0th
	elif(number == 1):
		return str(number)+"st" # 1st
	elif(number == 2):
		return str(number)+"nd" # 2nd
	elif(number == 3):
		return str(number)+"rd" # 3rd
	elif(number >= 4):
		return str(number)+"th" # 4th, 5th, etc.
	else:
		return "ERROR"

# Returns a date using just numbers, e.g. 03/04/2148
func get_date_numbers():
	if(days_first):
		return str("%02d" % day)+"/"+str("%02d" % month)+"/"+str(year)
	return str("%02d" % month)+"/"+str("%02d" % day)+"/"+str(year)

# Returns a date using a short month name, e.g. Mar 4th, 2148
func get_date_short():
	if(days_first):
		return get_ordinal_suffix(day)+" "+month_shorthand[month]+", "+str(year)
	return month_shorthand[month]+" "+get_ordinal_suffix(day)+", "+str(year)

# Returns a date using a short month name and numbers, e.g. Mar 04, 2148
func get_date_short_numbers():
	if(days_first):
		return str("%02d" % day)+" "+month_shorthand[month]+", "+str(year)
	return month_shorthand[month]+" "+str("%02d" % day)+", "+str(year)

# Returns a date using the full month name, e.g. March 4th, 2148
func get_date():
	if(days_first):
		return get_ordinal_suffix(day)+" "+month_names[month]+", "+str(year)
	return month_names[month]+" "+get_ordinal_suffix(day)+", "+str(year)
