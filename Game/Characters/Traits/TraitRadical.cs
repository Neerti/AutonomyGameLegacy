﻿using System;
using Autonomy.Characters.Opinions;
using System.Collections.Generic;
using Autonomy.Ideologies;

namespace Autonomy.Characters.Traits
{
	// Radical people receive significant bonuses to their personal stats.
	// However they also tend to make them volatile, as they tend to hate
	// everyone who disagrees with them. Opposing Radicals will also hate them.
	public class TraitRadical : TraitPersonality
	{
		public TraitRadical()
		{
			_id = TraitID.Radical;
			display_name = "Radical";
			description = "They are strongly committed to their ideological " +
				"beliefs, and willing to go to great lengths for their cause. " +
				"Unfortunately, this doesn't tend to win them many " +
				"friends.";
			// This needs to be rewritten to be clearer later.
			effects = "Signifcant boost to all stats. " +
				"More resistant to Ideology Drift. " +
				"Greatly increased opinion malus for opposing ideology. " +
				"Significant opinion boost with people in same " +
				"ideology. Significantly decreased opinion with others " +
				"in same ideology group, worsened if in opposing ideology, " +
				"and extremely lowered opinion with opposing Radicals.";
			quote = "Todo - Neerti";
			ideology_drift_defence = 2;
			ideology_opinion_malus_modifier = 2.0f;
			conflicting_traits = new List<Enum> { TraitID.Pragmatic };
		}
		
		// How we see others.
		public override OpinionModifier? GetOpinionIntrinsic(Character _us, Character _them)
		{
			switch (_us.IsAligned(_them))
			{
				case Ideology.Alignment.Identical:
					return new OpinionModifier(50, _them + " is wise to follow the correct ideology.");
				case Ideology.Alignment.Allied: // Radicals don't like it if you're not exactly aligned with them.
					return new OpinionModifier(-50, _them + " is not a true beliver of " + _us.Ideology.Ideology_group + ".");
				case Ideology.Alignment.Opposed: // Radicals really don't ike it if you oppose them.
					if (_them.HasTrait(TraitID.Radical))
					{
						// Opposing Radicals are enemies for life.
						return new OpinionModifier(-200, _them + " will corrupt the ill-informed with their lies!");
					}
					return new OpinionModifier(-100, _them + "'s beliefs are repugnant and objectively wrong.");
			}
			// This shouldn't reach here but Mono complains if this isn't here, and case default: isn't allowed in C# 7.0 .
			return base.GetOpinionIntrinsic(_us, _them);
		}
		
		// How others see us.
		public override OpinionModifier? GetOpinionExtrinsic(Character _us, Character _them)
		{
			if (_us.IsAligned(_them) is Ideology.Alignment.Identical)
			{
				return new OpinionModifier(20, _us + " is a champion of " + _us.Ideology + "."); ;
			}
			
			return base.GetOpinionExtrinsic(_us, _them);
		}
	}
}
