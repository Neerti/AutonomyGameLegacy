using System;
using System.Collections.Generic;
using Autonomy.Characters.Opinions;
using Autonomy.Characters.Species;

namespace Autonomy.Characters.Traits
{
	/// <summary>
	/// Traits are essentially a modifier for <see cref="Character"/>s.
	/// Changes can include stat adjustments, opinion modifiers, 
	/// AI behaviour changes, or just flavor.
	/// </summary>
	public abstract class Trait
	{
		// UI/Bookkeeping things.
		public enum TraitID
		{
			Base,
			// AI only
			Sapient,
			// Personality
			OpenMinded,
			CloseMinded,
			Friendly,
			Cold,
			Radical,
			Pragmatic,
			// Cybernetic
			ProstheticArm,
			ProstheticLeg,
			BCI,
			// Biological
			Longevity
		}
		
		/// <summary>
		/// Unique enum ID for bookkeeping.
		/// </summary>
		protected Enum _id = TraitID.Base;
	
		public Enum ID => _id;
	
		/// <summary>
		/// The name of the Trait that is shown to the player.
		/// </summary>
		protected string display_name;
	
		public string Display_name => display_name;
		
		public override string ToString()
		{
			return display_name;
		}
		
		/// <summary>
		/// A short description about the Trait.
		/// </summary>
		protected string description;
	
		public string Description => description;
	
		/// <summary>
		/// Description of mechanical effects the Trait actually has.
		/// </summary>
		protected string effects;
	
		public string Effects => effects;
	
		/// <summary>
		/// A short quote about the Trait, for flavor.
		/// </summary>
		protected string quote;
	
		public string Quote => quote;
	
		/// <summary>
		/// Determines if the trait should not be initially visible to others.
		/// Characters holding the trait will always see it.
		/// Other Characters can discover hidden Traits with various means.
		/// </summary>
		protected bool _hidden;
	
		public bool Hidden => _hidden;
	
		/// <summary>
		/// List of muturally exclusive Trait IDs, that conflict with this Trait.
		/// </summary>
		public List<Enum> conflicting_traits = new List<Enum>();
		
		/// <summary>
		/// List of traits that must already be on a <see cref="Character"/> 
		/// before this one can be applied. If empty, no traits are required.
		/// </summary>
		public List<Enum> prerequisite_traits = new List<Enum>();

		// Regular effects.

		// Lifespan
		/// <summary>
		/// Increases or decreases the average lifespan of a character. 
		/// For AI characters, it instead makes breakdowns more or less likely.
		/// </summary>
		protected int lifespan_modifier;

		public int Lifespan_modifier => lifespan_modifier;
		
		// Idealogy-related
		
		/// <summary>
		/// Makes changing idealogies easier or harder. Positive numbers 
		/// make the Character more resistant to change. Negative numbers 
		/// make it easier.
		/// </summary>
		protected int ideology_drift_defence;
	
		/// <summary>
		/// Modifies how much Characters will dislike each other for having 
		/// Idealogies which conflict.
		/// </summary>
		protected float ideology_opinion_malus_modifier = 1.0f;

		public float Ideology_opinion_malus_modifier => ideology_opinion_malus_modifier;

		/// <summary>
		/// Type which determines if a Trait is only valid on a certain 
		/// type of Character, such as an AI Character or Human.
		/// </summary>
		protected Enum species_restricted = CharacterSpecies.SpeciesID.Base;
	
		public Enum Species_restricted => species_restricted;
		
		// Override this for conditional opinion modifiers.
		
		/// <summary>
		/// Gets a <see cref="OpinionModifier"/> from a <see cref="Trait"/> based 
		/// on how <paramref name="_us"/> feels about <paramref name="_them"/> due 
		/// to holding this trait.
		/// </summary>
		/// <returns>The opinion modifier struct, or null.</returns>
		/// <param name="_us">The <see cref="Characters"/> holding this trait.</param>
		/// <param name="_them">The <see cref="Characters"/> who _us is evaluating.</param>
		public virtual OpinionModifier? GetOpinionIntrinsic(Character _us, Character _them)
		{
			return null;
		}
		
		/// <summary>
		/// Gets a <see cref="OpinionModifier"/> from a <see cref="Trait"/> 
		/// based on how <paramref name="_them"/> feels about 
		/// <paramref name="_us"/> for holding this trait.
		/// </summary>
		/// <returns>The opinion modifier struct, or null.</returns>
		/// <param name="_us">The <see cref="Characters"/> holding this trait.</param>
		/// <param name="_them">The <see cref="Characters"/> who is evaluating _us.</param>
		public virtual OpinionModifier? GetOpinionExtrinsic(Character _us, Character _them)
		{
			return null;
		}
	}
}
