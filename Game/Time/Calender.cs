using Godot;
using System;

/// <summary>
/// A class to hold and advance the in-game date, using <see cref="DateTime"/>.
/// </summary>
public class Calender : Node
{
	DateTime _date;

	public DateTime Date => _date;

	[Signal]
	delegate void DayPassed();

	[Signal]
	delegate void MonthPassed();

	[Signal]
	delegate void YearPassed();

	// TODO: Interim stub, remove later when UI/Planet orbits are made to use csharp.
	[Signal]
	delegate void DateChanged(string current_date);
	
	public Calender()
	{
		_date = new DateTime(2100, 1, 1);
	}
	
	public Calender(DateTime _new_date)
	{
		_date = _new_date;
	}

	public override void _Ready()
	{
		Timer my_timer = (Timer)GetNode("../Ticker");
		my_timer.Connect("timeout", this, nameof(Tick));
	}

	/// <summary>
	/// Advances the in-game date by one day, and emits the relevant 
	/// signals to other nodes.
	/// </summary>
	public void Tick()
	{
		DateTime last_day = _date;
		_date = _date.AddDays(1);
		EmitSignal(nameof(DayPassed));
		// Interim stub.
		EmitSignal(nameof(DateChanged), _date.ToShortDateString());
		//GD.Print("Current date: "+_date.ToShortDateString());
		
		// Test if it is a new month.
		if (_date.Month != last_day.Month)
		{
			EmitSignal(nameof(MonthPassed));
		}
		// Test if it is a new year.
		if(_date.Year != last_day.Year)
		{
			EmitSignal(nameof(YearPassed));
		}
	}
}
