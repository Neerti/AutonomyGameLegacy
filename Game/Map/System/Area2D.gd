extends Area2D

func _ready():
	set_process_input(true)
	pass

# Detects clicks from the user.
func _input_event(viewport, event, shape_idx):
	if event is InputEventMouseButton:
		if event.is_pressed() and event.button_index == BUTTON_LEFT:
			open_system()

# Asks the Map node to open the System attached to its parent.
func open_system():
	print("Opening ", $"..".name, "'s SystemPin.")
	var system_to_send = $"..".get_system()
	var map = $"/root/Main/Map"
	map.enter_system_map(system_to_send)
	pass