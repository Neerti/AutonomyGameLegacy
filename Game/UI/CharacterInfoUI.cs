using Godot;
using System;
using Autonomy.Globals;

namespace Autonomy.UI
{
	public class CharacterInfoUI : Panel
	{
		public override void _GuiInput(InputEvent @event)
		{
			// TODO: Compress the massive amount of ifs.
			if (@event is InputEventMouseButton _click)
			{
				if (_click.ButtonIndex == (int)ButtonList.Left && _click.Pressed)
				{
					if(!(Global.player is null))
					{
						GD.Print("Building window.");
						WindowManager WM = (WindowManager)GetNode("/root/Main/UI/WindowManager");
						WM.MakeCharacterWindow(Global.player);
						
					}
				}
			}
		}
	}
}
