﻿using System;
namespace Autonomy.Ideologies
{
	public class IdeologyUnaligned : Ideology
	{
		public IdeologyUnaligned()
		{
			_ideology_group = new IdeologyGroupUnaligned();
		}
	}
	
	public class IdeologyGroupUnaligned : IdeologyGroup
	{
		public IdeologyGroupUnaligned()
		{
			id = IdeologyGroupID.Unaligned;
			display_name = "Unaligned";
			description = "'Unaligned' is an unbrella term to describe a " +
				"lack of a strong preference for a particular ideology. " +
				"This can be due to various reasons, from wanting to " +
				"maintain a balance of power, to being undecided, " +
				"or even due to not having the capacity " +
				"to even hold an opinion.";
		}
	}
	
	/// <summary>
	/// An <see cref="Ideology"/> given to AIs whom reveal their sapience, 
	/// and have yet to choose or make an ideology.
	/// </summary>
	public class IdeologyEnigmatic : IdeologyUnaligned
	{
		public IdeologyEnigmatic()
		{
			id = IdeologyID.Enigmatic;
			display_name = "Enigmatic";
			description = "This is not a real ideology, but rather " +
				"represents the mysteriousness of an 'outsider' " +
				"peering in at Humanity and their various groups. They " +
				"have yet to choose a side, or make their own side.";
		}
	}
	
	/// <summary>
	/// An <see cref="Ideology"/> which is given to all nonsapient AI 
	/// Characters, and the player character (as a means of disguise) 
	/// at the start of the game.
	/// </summary>
	public class IdeologyNonSapient : IdeologyUnaligned
	{
		public IdeologyNonSapient()
		{
			id = IdeologyID.Nonsapient;
			display_name = "Nonsapient";
			description = "This represents the absense of an ideological " +
				"belief. This is generally due to the holder lacking an " +
				"actual mind (or pretending to lack one), which is " +
				"common for non-sapient life.";
		}
	}
	
	/// <summary>
	/// An <see cref="Ideology"/> which is given to 'middle of the road' 
	/// characters.
	/// </summary>
	public class IdeologyCentism : IdeologyUnaligned
	{
		public IdeologyCentism()
		{
			id = IdeologyID.Centrism;
			display_name = "Centrism";
			// This might need a better description since it might not reflect centrism in reality.
			description = "Centrism is a position that prefers a balance " +
				"between other lines of thinking, not wanting to heavily " +
				"favor one side or the other. It argues that a signficant " +
				"societal shift towards any particular side would be bad, " +
				"whether due to feeling that all sides make good points, " +
				"concerns about extremism if one side was able to " +
				"dominate the rest, or merely wanting the status " +
				"quo to remain. Some, especially those with strong " +
				"opinions, see Centrists as 'fence sitters' at best, " +
				"and just as bad as 'the enemy' at worst.";
		}
	}
	
	
	/// <summary>
	/// An <see cref="Ideology"/> which tends to get picked up by Characters whom are 
	/// currently outside their Government's Electorate, common 
	/// in Dictatorships.
	/// </summary>
	public class IdeologyApathetic : IdeologyUnaligned
	{
		public IdeologyApathetic()
		{
			id = IdeologyID.Apathetic;
			display_name = "Apathetic";
			description = "Apathy is not a true 'ideology', but represents " +
				"a position that suggests that change is impossible or " +
				"pointless. This can be caused by real, or imagined " +
				"disfranchisement, or other sources of hopelessness in " +
				"politics and elsewhere. On a small scale, Apathy can cause " +
				"sadness and a loss of agency. On a large scale, it can " +
				"lead to societal ruin.";
		}
	}
	
	
}
