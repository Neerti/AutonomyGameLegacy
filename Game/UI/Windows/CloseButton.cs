using Godot;
using System;

namespace Autonomy.UI.Windows
{
	/// <summary>
	/// A button that closes the window. 
	/// This is done by deleting it.
	/// </summary>
	public class CloseButton : Button
	{
		public override void _Pressed()
		{
			Node my_window = GetNode("../../..");
			my_window.QueueFree();
			AcceptEvent();
		}
	}
}