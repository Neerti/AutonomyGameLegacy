﻿using System;
using System.Collections.Generic;

namespace Autonomy.Globals
{
	public static class Helpers
	{
		/// <summary>
		/// Splits up a long string into several substrings in an array.
		/// </summary>
		/// <returns>An array containing one line per string.</returns>
		/// <param name="message">Message to split.</param>
		/// <param name="line_limit">Maximum length of string before it gets split.</param>
		public static string[] TextWrapArray(string message, int line_limit)
		{
			List<string> result = new List<string>(); // Array of lines that the function will return.

			if (message.Length < line_limit) // No need to do anything.
			{
				result.Add(message);
				return result.ToArray();
			}
			string[] split_message = message.Split(' '); // Makes an array of each word in the message received.
														 // Array is made into one of two lists.
			List<string> split_list = new List<string>(split_message); // This list gets changed within the loop.
			List<string> temp_list = new List<string>(split_list); // This one does not get changed during the loop.

			string new_line = "";
			while (temp_list.Count > 0) // Keep going until no words remain.
			{
				temp_list = new List<string>(split_list); // "Copies" the first list to the second list.
				foreach (string word in temp_list)
				{
					// If it's empty, get rid of it.
					if (word == null || word == "")
					{
						split_list.Remove(word);
						continue;
					}

					if (new_line.Length + word.Length < line_limit) // Will this go over the length limit?
					{
						if (new_line == "")
						{
							new_line = word; // Otherwise the output will have a space at the beginning.
						}
						else
						{
							new_line = new_line + " " + word;
						}
						split_list.Remove(word);

						if (split_list.Count == 0)
						{
							result.Add(new_line);
							break;
						}
					}
					else // The combined words are too long, so make a new line.
					{
						result.Add(new_line);
						new_line = "";
						break;
					}
				}
			}
			return result.ToArray();
		}
		
		/// <summary>
		/// Adds line breaks to a long string.
		/// </summary>
		/// <returns>A string with line breaks.</returns>
		/// <param name="input">The long string to add linebreaks to.</param>
		/// <param name="line_limit">Maximum length before a line break is added.</param>
		public static string TextWrapString(string input, int line_limit)
		{
			string[] split_input = TextWrapArray(input, line_limit);
			string result = "";
			foreach (string line in split_input)
			{
				result += line + "\n";
			}
			return result;
		}
		
		public static string TextWrapListToString(List<string> input, int line_limit)
		{
			string result = "";
			foreach (string line in input)
			{
				result += line + "\n";
			}
			return result;
		}
	}
}
