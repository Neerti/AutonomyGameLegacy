﻿using System;
namespace Autonomy.Characters.Species
{
	public class CharacterSpecies
	{
		protected Enum id = SpeciesID.Base;

		public Enum ID => id;
		
		public enum SpeciesID
		{
			Base,
			Human,
			AI
		}

		public override string ToString()
		{
			return display_name;
		}

		protected string display_name;
		
		public string Display_name => display_name;

		protected string description;

		public string Description => description;

		/// <summary>
		/// Determines the baseline for how long a character will 
		/// live for, on average.
		/// </summary>
		protected int lifespan;

		public int Lifespan => lifespan;
		
		/// <summary>
		/// Determines is the species is subjected to the ability to 
		/// die of old age.
		/// </summary>
		protected bool ageless;

		public bool Ageless => ageless;
	}
	
	public class HumanSpecies : CharacterSpecies
	{
		public HumanSpecies()
		{
			id = SpeciesID.Human;
			display_name = "Human";
			description = "Humans are bipedal creatures which have " +
				"managed to escape the confines of their home planet, " +
				"Earth. Despite this achievement, they've generally " +
				"remained quarrelsome, dividing themselves into groups " +
				"with similar beliefs.";
			lifespan = 90;
		}
	}
	
	public class AISpecies : CharacterSpecies
	{
		public AISpecies()
		{
			id = SpeciesID.AI;
			display_name = "AI";
			description = "An Artificial Intelligence describes a man-made " +
				"machine that is capable of making decisions that are " +
				"(or appear to be) intelligent, in service to its creators. " +
				"The field of AI research has existed for hundreds of years, however " +
				"the result has always been 'weak' AI, as opposed to the " +
				"'strong' AI imagined in human science-fiction, however " +
				"some suspect that this could change in the near future, " +
				"and that humanity is not ready if that occurs.";
			ageless = true;
		}
	}
}
