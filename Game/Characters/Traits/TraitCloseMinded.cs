﻿using System;
using System.Collections.Generic;
using Autonomy.Characters.Opinions;

namespace Autonomy.Characters.Traits
{
	// Close-minded people are less popular than their more accepting counterparts.
	// However, they hold onto their beliefs harder than normal.
	public class TraitCloseMinded : TraitPersonality
	{
		public TraitCloseMinded()
		{
			_id = TraitID.CloseMinded;
			display_name = "Close Minded";
			description = "They go out of their way to avoid ideas which don't " +
				"agree with their viewpoints.";
			effects = "Reduces general opinon. More opinion malus from " +
				"a conflicting Idealogy. More resistant to Idealogy Drift.";
			quote = "Todo - Neerti";
			ideology_drift_defence = 1;
			ideology_opinion_malus_modifier = 1.5f;
			conflicting_traits = new List<Enum> { TraitID.OpenMinded };
		}
		
		public override OpinionModifier? GetOpinionExtrinsic(Character _us, Character _them)
		{
			return new OpinionModifier(-5, _us + " is a simple closeminded fool.");
		}
	}
}