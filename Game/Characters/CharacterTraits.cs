﻿using System;
using Godot;
using Autonomy.Characters.Traits;
using Autonomy.Characters.Species;

namespace Autonomy.Characters
{
	public partial class Character : Node
	{
		/// <summary>
		/// Checks if this Character has a specific Trait, with a unique ID.
		/// </summary>
		/// <returns><c>true</c>, if trait was found, <c>false</c> otherwise.</returns>
		/// <param name="_trait_id">Trait identifier.</param>
		public bool HasTrait(Enum _trait_id)
		{
			foreach (Trait trait in _traits)
			{
				if (trait.ID.Equals(_trait_id))
				{
					return true;
				}
			}
			return false;
		}

		public bool CanAddTrait(Trait _new_trait)
		{
			foreach (Trait T in _traits)
			{
				// Test for duplicate trait types.
				if (HasTrait(_new_trait.ID))
				{
					// Duplicate traits are not allowed.
					GD.Print(this + ".CanAddTrait(" + _new_trait + ") | Rejecting duplicate.");
					return false;
				}

				// Check for traits that conflict against us.

				foreach (Enum their_conflict in T.conflicting_traits)
				{
					if (_new_trait.ID.Equals(their_conflict))
					{
						// Character has a trait that is conflicting. (Theirs)
						GD.Print(this + ".CanAddTrait(" + _new_trait + ") | Rejecting conflicting trait (theirs).");
						return false;
					}
				}

				foreach (Enum our_conflict in _new_trait.conflicting_traits)
				{
					if (T.ID.Equals(our_conflict))
					{
						// Trait being added is conflicting with a trait on the Character. (Us)
						GD.Print(this + ".CanAddTrait(" + _new_trait + ") | Rejecting conflicting trait (us).");
						return false;
					}
				}
			}
			// Check if any prerequisite traits are needed.
			if (_new_trait.prerequisite_traits.Count > 0)
			{
				bool prereq_fulfilled = true;
				foreach (Enum prereq in _new_trait.prerequisite_traits)
				{
					if (!HasTrait(prereq))
					{
						prereq_fulfilled = false;
						GD.Print(this + ".CanAddTrait(" + _new_trait + ") | Trait " + prereq + " required and was not found.");
						break;
					}
				}
				if (!prereq_fulfilled)
				{
					GD.Print(this + ".CanAddTrait(" + _new_trait + ") | Lacked prerequisite traits.");
					return false;
				}
			}

			// Test if Character type restricts us.
			if (!_new_trait.Species_restricted.Equals(CharacterSpecies.SpeciesID.Base))
			{
				// Trait is not allowed on this character type.
				if (!_new_trait.Species_restricted.Equals(Species.ID))
				{
					GD.Print(this + ".CanAddTrait(" + _new_trait + ") | Wrong species.");
					return false;
				}
			}
			GD.Print(this + ".CanAddTrait(" + _new_trait + ") | Passed all checks.");
			return true;
		}

		/// <summary>
		/// Attempts to add a Trait to this Character.
		/// </summary>
		/// <returns><c>true</c>, if trait was added, <c>false</c> otherwise.</returns>
		/// <param name="_new_trait">New trait instance to try to add.</param>
		public bool AddTrait(Trait _new_trait)
		{
			if (CanAddTrait(_new_trait))
			{
				// Nothing went wrong, add the trait.
				_traits.Add(_new_trait);
				GD.Print(this + ".AddTrait(" + _new_trait + ") | Added trait.");
				return true;
			}
			GD.Print(this + ".AddTrait(" + _new_trait + ") | Failed to add trait.");
			return true;
		}
	}
}
