extends Node2D

signal zoom_adjusted

const CAMERA_SPEED = 1000
const ZOOM_STEPS = [0.5, 1.0, 2.0, 4.0, 8.0, 16.0, 32.0, 64.0, 128.0, 256.0, 512.0, 1024.0, 2048.0, 4096]

#const PIXELS_PER_AU_STEPS = [64000, 32000, 16000, 8000, 4000, 2000, 1000, 500, 250, 100, 50, 10, 1, 0.1, 0.01, 0.001, 0.0001]
const PIXELS_PER_AU_STEPS = [
		32768,
		16384,
		8192, # Lunar distances
		4096,
		2048,
		1024,
		512,
		256,
		128, # Inner System
		64,
		32,
		16,
		8, # Outer System
		4,
		2,
		1,
		0.5, # Interstellar distances
		0.25,
		0.125,
		0.0625
		]

var pixels_per_AU = 256

var zoom_step_index = 7 # At 1.0.

func _process(delta):
	var velocity = Vector2()
	
	# X axis.
	if Input.is_action_pressed("ui_right"):
		velocity.x += 1
	if Input.is_action_pressed("ui_left"):
		velocity.x -= 1
	
	# Y axis.
	if Input.is_action_pressed("ui_down"):
		velocity.y += 1
	if Input.is_action_pressed("ui_up"):
		velocity.y -= 1
	
	# Handle velocity normalization.
	if velocity.length() > 0:
		velocity = velocity.normalized() * CAMERA_SPEED * $Camera2D.zoom.x
	
	# Apply velocity to camera.
	position += velocity * delta
	
	# Future: Limit position.
#	position.x = clamp(position.x, 0, screensize.x)
#	position.y = clamp(position.y, 0, screensize.y)
#	print(position)

func _input(event):
	# Wheel Up Event
	if event.is_action_pressed("zoom_in"):
		_zoom_camera(-1)
	# Wheel Down Event
	elif event.is_action_pressed("zoom_out"):
		_zoom_camera(1)

# Zoom Camera
func _zoom_camera(dir):
#	var clamped_zoom = clamp(
#		zoom_step_index + 1 * dir,
#		0,
#		ZOOM_STEPS.size()-1
#		)
	var clamped_zoom = clamp(
		zoom_step_index + 1 * dir,
		0,
		PIXELS_PER_AU_STEPS.size()-1
		)
	var old_zoom = pixels_per_AU
	zoom_step_index = clamped_zoom
#	$Camera2D.zoom = Vector2(ZOOM_STEPS[zoom_step_index], ZOOM_STEPS[zoom_step_index])
	pixels_per_AU = PIXELS_PER_AU_STEPS[zoom_step_index]
	emit_signal("zoom_adjusted") # To update everything to the correct scale.
	print(pixels_per_AU)
	
	# Adjust camera position when zoomed to be less disorienting.
	var offset_factor = float(pixels_per_AU) / float(old_zoom)
	print("offset_factor is: ", offset_factor)
	position = position * offset_factor
	

