﻿using Autonomy.Characters;
using Autonomy.Ideologies;
using Autonomy.Characters.Species;

/// <summary>
/// Class for Human characters.
/// </summary>
public class CharacterHuman : Character
{
	public CharacterHuman()
	{
		species = new HumanSpecies();
	}
}
