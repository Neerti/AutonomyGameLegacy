extends Node

# The game map uses two seperate maps to display the world.
# The first map is the Interstellar map, which is the 'main' map, and holds a bunch of star positions, 
# and is permanent.
# The second map is the currently visible system inside the Systems node, if one is visible.
# If a system is entered, the main map becomes invisible and a specific system inside the Systems
# node becomes visible. When exiting the system, the inverse occurs.

# Used to keep track of map state.
var inside_system_map = false

# Current system being viewed, or last viewed system if inside_system_map is false.
var current_system = null

func enter_system_map(system_being_entered):
	# Set state.
	inside_system_map = true
	current_system = system_being_entered
	$Interstellar.hide()
	
	# Show the system we're entering into.
	system_being_entered.show()
	
	# Center the camera to origin.
	var camera = $"/root/Main/Camera"
	camera.position = Vector2(0, 0)

func exit_system_map():
	# Set state.
	if current_system:
		current_system.hide()
		
		# Center the camera to the system we just exited.
		var camera = $"/root/Main/Camera"
		camera.position = current_system.my_map_pin.get_ref().position
#		print(current_system.my_map_pin.get_ref().position)
		
#		current_system = null
	inside_system_map = false
	$Interstellar.show()
	

func _unhandled_input(event):
	if event.is_action_pressed("view_map"):
		if inside_system_map:
			exit_system_map()
		elif current_system:
			enter_system_map(current_system)