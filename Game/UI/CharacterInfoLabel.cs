using Godot;
using System;
using Autonomy.Characters;
using Autonomy.Globals;

namespace Autonomy.UI
{
	public class CharacterInfoLabel : Label
	{
		public override void _Process(float delta)
		{
			UpdateText();
		}

		public void UpdateText()
		{
			Character my_character = Global.player;
			if (my_character is null)
			{
				Text = "(Observer)";
				return;
			}
			Text = my_character.ToString();
			
		}
		
	}
}
