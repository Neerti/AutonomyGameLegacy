using Godot;
using System;
using Autonomy.Characters;
using Autonomy.Globals;

namespace Autonomy.UI.Windows
{
	public class CharacterWindowContainer : VBoxContainer
	{
		
		/// <summary>
		/// The character being displayed in the window.
		/// </summary>
		Character _subject;
		
		/// <summary>
		/// Gets or sets the subject.
		/// </summary>
		/// <value>The Character.</value>
		public Character Subject { get => _subject; }
		
		public void SetSubject(Character _new_subject)
		{
			_subject = _new_subject;
			UpdateInfo();
		}
		
		
		/// <summary>
		/// Writes subject information to relevant labels. Call if subject changes.
		/// </summary>
		public void UpdateInfo()
		{
			if (_subject is null)
			{
				// TODO: Make an Exception to throw here.
				GD.Printerr("Error: CharacterWindow subject was null!");
				return;
			}
			// Update the title. The title holds the character's name.
			UpdateTitle();
			
			// Species.
			UpdateSpecies();
			
			// Age.
			UpdateAge();
			
			// Ideology.
			UpdateIdeology();

			// Traits.
			UpdateTraits();
			
			// Opinion.
			UpdateOpinion();



		}
		
		public void UpdateTitle()
		{
			Window _my_window = (Window)GetNode("../..");
			_my_window.SetTitleText(_subject.ToString());
		}
		
		
		public void UpdateSpecies()
		{
			Label _species_label = (Label)GetNode("SpeciesLabel");
			_species_label.Text = _subject.Species.ToString();
			_species_label.HintTooltip = Helpers.TextWrapString(_subject.Species.Description, 60);
		}
		
		
		public void UpdateAge()
		{
			// Not Yet Implemented.
			return;
		}
		
		
		public void UpdateIdeology()
		{
			Label _ideology_label = (Label)GetNode("IdeologyContainer/IdeologyList/Ideology");
			_ideology_label.Text = _subject.Ideology.ToString();
			_ideology_label.HintTooltip = Helpers.TextWrapString(_subject.Ideology.Description, 60);

			Label _ideology_group_label = (Label)GetNode("IdeologyContainer/IdeologyList/IdeologyGroup");
			_ideology_group_label.Text = "(" + _subject.Ideology.Ideology_group + ")";
			_ideology_group_label.HintTooltip = Helpers.TextWrapString(_subject.Ideology.Ideology_group.Description, 60);
		}
		
		
		public void UpdateTraits()
		{
			VBoxContainer trait_list = (VBoxContainer)GetNode("TraitsContainer/TraitsList");
			// Delete the old labels.
			foreach (Label trait_label in trait_list.GetChildren())
			{
				trait_label.QueueFree();
			}
			
			// Add the new, up to date labels.
			foreach (var T in _subject.Traits)
			{
				Label new_label = new Label
				{
					Text = T.Hidden ? T.Display_name + " (Hidden)" : T.Display_name,
					Align = Label.AlignEnum.Right,
					MouseFilter = MouseFilterEnum.Stop, // For tooltips.
					HintTooltip = Helpers.TextWrapString(T.Description, 60)
				};
				trait_list.AddChild(new_label);
			}
			//trait_list.Update();
			//QueueSort();
			
		}
		
		
		public void UpdateOpinion()
		{
			Label _opinion_label = (Label)GetNode("OpinionContainer/Opinion");
			bool opinion_hide;

			if (Global.player != null)
			{
				if (_subject.Equals(Global.player))
				{
					// No point getting the opinion of ourselves.
					opinion_hide = true;
				}
				else
				{
					opinion_hide = false;
					_opinion_label.Text = _subject.GetOpinionValue(Global.player).ToString();
					_opinion_label.HintTooltip = Helpers.TextWrapListToString(_subject.GetOpinionPretty(Global.player), 60);
				}
			}
			else
			{
				// We're in 'observer mode'.
				opinion_hide = true;
			}
			Control opinion_container = (Control)GetNode("OpinionContainer");
			opinion_container.Visible = !opinion_hide;
		}
		
	}
}