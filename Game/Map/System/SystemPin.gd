extends Position2D

# This points to a System object, as well as holding a position on the Interstellar Map.

var my_weakref = null

func bind_to_system(var _new_system):
	my_weakref = weakref(_new_system)
	name = _new_system.name
	$Sprite/NameLabel.text = name
	
	_new_system.bind_to_system_pin(self)

func get_system():
	return my_weakref.get_ref()