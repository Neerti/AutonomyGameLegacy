﻿using System;
using Autonomy.Characters.Species;

namespace Autonomy.Characters.Traits
{
	// This trait is required for advanced cybernetic traits in humans.
	// AIs do not need this as they are already a machine.
	// Those with a BCI also open themselves up to risks that come with 
	// having a computer wired into your brain.
	public class TraitBCI : TraitCybernetic
	{
		public TraitBCI()
		{
			_id = TraitID.BCI;
			display_name = "BCI";
			description = "A Brain-Computer Interface, or BCI, allows for " +
				"machinery, in or outside of the body, to connect directly " +
				"to the human brain, blurring the lines between man" +
				"and machine. It functions by translating the brain's " +
				"neural processes into traditional digital signals, allowing " +
				"an interface between the two sides. " +
				"Advanced cybernetics generally require a BCI to have been " +
				"installed in order to install the cybernetic.";
			effects = "Prerequisite for advanced cybernetic augmentations.";
			species_restricted = CharacterSpecies.SpeciesID.Human;
			advanced = true; // Will bother non-transhumanists.
		}
	}
}
