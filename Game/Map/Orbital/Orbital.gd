extends Position2D

# This implements a fake circular orbit around the node's parent.
# Maybe someday it can be refactored to include ellipical orbits.
# This can be used for planets, moons, space stations, and anything else.

# Physics constants;

# Mass
# Might scale up the base unit from kg to something larger.
const SOLAR_MASS = 1.98855 * pow(10, 30) # kg
const EARTH_MASS = 5.9722 * pow(10, 24) # kg
const LUNAR_MASS = 7.3459 * pow(10, 22) # kg

# Distance.
const ASTRONOMICAL_UNIT = 149597870700.0 # km

# Speed.
const SPEED_OF_LIGHT = 299792.458 # km

# Time.
const DAYS_IN_YEAR = 365.25

const ORBIT_SPEED_MULTIPLIER = 1

const MIN_DISTANCE_TO_HIDE = 15 # If the orbital is this many pixels to its parent or closer, it will be hidden.

var parent = null # Node's parent, stored to save fetching it constantly.

export (bool) var retrograde = false # If true, will rotate in reverse.

export (float) var orbital_distance = 1 # How far from parent to orbit to, in AU. In-game, this is scaled based on zoom.

var _orbital_period = 1 # How long it takes to orbit once.
var _current_angle = 0 # Radians.
var initial_angle = null # Ditto.

var time = 0 # Todo: Remove in favor of universal time.

func setup(_name, _distance):
	name = _name
	orbital_distance = _distance

func _ready():
	parent =  get_parent()
	if(orbital_distance == 0): # Static orbital (Most likely the star).
		set_process(false)
		return
	
	_orbital_period = calculate_orbital_period(orbital_distance)
	if(initial_angle == null):
		initial_angle = TAU * randf() # Randomize the starting angle if not set.
	
	update_orbit() # Update once to get the orbital out of the map's origin.
	
	# Connect the orbital to the ticker.
	var our_ticker = get_tree().get_root().get_node("Main/Time/Ticker")
	our_ticker.connect("timeout", self, "on_daily_tick")
	
	var our_camera = $"/root/Main/Camera"
	our_camera.connect("zoom_adjusted", self, "on_zoom_adjusted")

#	var speed_slider = get_tree().get_root().get_node("root/ui_root/top_right_group/date_panel/speed_slider")
#	speed_slider.connect("value_changed", self, "update_speed")

# Todo: Remove this
# Later on this will get processed as needed when within a certain distance of the player's camera.
# Since the angle is based on a time value, nothing will be lost if the orbit stops updating offscreen.
#func _process(delta):
#	time += (delta * ORBIT_SPEED_MULTIPLIER)
#	calculate_angle(time / DAYS_IN_YEAR)
#	update_orbital_position()

func on_daily_tick():
	time += 1.0
	if(time >= _orbital_period * DAYS_IN_YEAR):
#		print("Resetting time variable on ", name, ". It is currently :", time, " _orbital_period is currently: ", _orbital_period * DAYS_IN_YEAR)
		time -= _orbital_period * DAYS_IN_YEAR # To avoid long term overflow.
	update_orbit()

func on_zoom_adjusted():
	update_orbit()
	hide_if_too_close()

func update_orbit():
	calculate_angle(time / DAYS_IN_YEAR)
	update_orbital_position()
	$"OrbitPath".update()

func update_orbital_position():
	var pos = Vector2(
			sin(_current_angle + initial_angle) * orbital_distance * get_pixels_per_AU(),
			cos(_current_angle + initial_angle) * orbital_distance * get_pixels_per_AU()
			)
	position = pos

func hide_if_too_close():
	var offset = global_position - get_parent().global_position
	var dist = round(offset.length())
#	print(name, "'s distance from its parent, ", get_parent().name, ", is ", dist, " pixels.")
	if dist <= MIN_DISTANCE_TO_HIDE:
		visible = false
	else:
		visible = true

# One t is equal to one year.
func calculate_angle(t):
	if(retrograde):
		t = -t
	_current_angle = (t / _orbital_period) * TAU

# Returns orbital period.
# Input must be in AU.
# Output is in years.
func calculate_orbital_period(distance_from_parent):
	var period = pow(distance_from_parent, 3)
	period = sqrt(period)
	return period

func get_pixels_per_AU():
	var cam = $"/root/Main/Camera"
	return cam.pixels_per_AU

func scale_sprite(new_vector2):
#	$"Sprite".scale = scale * new_vector2
	pass
