extends Node
# This is in a 'Control' Node in order to allow a pause exemption without affecting Main's 
# children (IE literally the entire game).

# TODO: Remove this in favor of a main menu popup or something.
func _unhandled_input(event):
	if event.is_action_pressed("quit"):
		quit_game()

func quit_game():
	print("Quitting.")
	get_tree().quit()