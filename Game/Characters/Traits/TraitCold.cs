﻿using System;
using System.Collections.Generic;
using Autonomy.Characters.Opinions;

namespace Autonomy.Characters.Traits
{
	// Cold people are unfriendly, emotionally distant, and not concerned for others.
	// They tend to be disliked by everyone else except for others like them.
	public class TraitCold : TraitPersonality
	{
		public TraitCold()
		{
			_id = TraitID.Cold;
			display_name = "Cold";
			description = "They have little time for the petty concerns " +
				"of others.";
			effects = "Reduces general opinion. Improves opinion of others " +
				"with the same trait.";
			quote = "Todo - Neerti";
			conflicting_traits = new List<Enum>{ TraitID.Friendly };
		}
		
		public override OpinionModifier? GetOpinionIntrinsic(Character _us, Character _them)
		{
			if (_them.HasTrait(_id)) // Both have the same trait.
			{
				return new OpinionModifier(10, _us + " and " + _them + " respect each others' desire to keep to themselves.");
			}
			
			return base.GetOpinionIntrinsic(_us, _them);
		}
		
		public override OpinionModifier? GetOpinionExtrinsic(Character _us, Character _them)
		{
			if (_them.HasTrait(_id)) // Both have the same trait.
			{
				return base.GetOpinionExtrinsic(_us, _them);
			}
			return new OpinionModifier(-10, _us + " is unfriendly and distant.");
		}
	}
}