extends Node

# Reference to the player's current character object, if any.
# This is used by the UI to display the correct character's stats.
# Ideally, changing this on the fly would allow for character swapping.

# If null, the game will consider the player to be in 'observer mode'.
var player_character = null

