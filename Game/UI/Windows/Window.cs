using Godot;
using System;

namespace Autonomy.UI.Windows
{
	public class Window : PanelContainer
	{

		/// <summary>
		/// Changes the title bar's text without having to directly 
		/// fetch and access the specific label.
		/// </summary>
		/// <param name="_new_title">The new title to display.</param>
		public void SetTitleText(string _new_title)
		{
			Label _my_title_label = (Label)GetNode("Content/TitleBar/Bar/Title");
			_my_title_label.Text = _new_title;
		}
	}
}
